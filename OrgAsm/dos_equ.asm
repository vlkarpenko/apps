
Version         equ #00
ChDisk          equ #01
CurDisk         equ #02
DskInfo         equ #03
G_Entry         equ #04

BootDsk         equ #09
Create          equ #0a
Creat_N         equ #0b
Erase           equ #0d
Delete          equ #0e
Move            equ #0f
Rename          equ #10
Open            equ #11
Close           equ #12
Read            equ #13
Write           equ #14
Move_Fp         equ #15
Attrib          equ #16
Get_D_T         equ #17
Put_D_T         equ #18
F_First         equ #19
F_Next          equ #1a
MkDir           equ #1b
RmDir           equ #1c
ChDir           equ #1d
CurDir          equ #1e
SysTime         equ #21
SetTime         equ #22

WaitKey         equ #30
ScanKey         equ #31
EchoKey         equ #32
CtrlKey         equ #33
Edit            equ #34
K_Clear         equ #35

SetWin          equ #38
SetWin1         equ #39
SetWin2         equ #3a
SetWin3         equ #3b
FreeMem         equ #3c
GetMem          equ #3d
RetMem          equ #3e
SetMem          equ #3f

Exec            equ #40
Exit            equ #41
Wait            equ #42

GSWitch         equ #43
DosName         equ #44

SetVMod         equ #50
GetVMod         equ #51
Locate          equ #52
Cursor          equ #53
SelPage         equ #54
Scroll          equ #55
Clear           equ #56
RdChar          equ #57
WrChar          equ #58
WinCopy         equ #59
WinRest         equ #5a
PutChar         equ #5b
PChars          equ #5c
Res_Prn         equ #5d
CtrlPrn         equ #5e
Print           equ #5f

